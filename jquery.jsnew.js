(function($) {
    $.fn.calendar = function () {

        let format = {
            
        };
        
        function ISODate() {
            let year = d.getFullYear(),
                month = d.getMonth()+1,
                dt = d.getDate();

            if (dt < 10) {
                dt = '0' + dt;
            }
            if (month < 10) {
                month = '0' + month;
            }
        }








        let d                   = new Date(),
            calendar            = this;
        const dayOfWeek         = ['Cn','T2','T3','T4','T5','T6','T7'];
        //Create 2 checkbox
        let $textBox1 = $('<input>')
            .addClass('textBox1')
            .attr({'value':d.getDate() + " " + "tháng" + " " + (d.getMonth() + 1) + " " + "năm" + " " + d.getFullYear()})
            .on('click',function() {
                // $(this).closest('.editcalendar').find('.tab')
                //     .show()
                //     .removeClass("right")
                //     .addClass("left");
                clickTextBox1($(this));

            })
            .data({'date':d.getDate(),'month':d.getMonth()+1,'year':d.getFullYear()});

        let $textBox2 = $('<input>')
            .addClass('textBox2')
            .attr({'value':d.getDate() + " " + "tháng" + " " + Number(d.getMonth() + 1) + " " + "năm" + " " + d.getFullYear()})
            .on('click',function() {
                // $(this).closest('.editcalendar').find('.tab')
                //     .show()
                //     .removeClass("left")
                //     .addClass("right");
                clickTextBox2($(this));
            })
            .data({'date':d.getDate(),'month':d.getMonth()+1,'year':d.getFullYear()});
        let $textBoxZone1 = $('<div>')
            .prop('id','zone1')
            .text('Ngày bắt đầu')
            .append($textBox1);
        let $textBoxZone2 = $('<div>')
            .prop('id','zone2')
            .text('Ngày kết thúc')
            .append($textBox2);
        let $textBoxZone = $('<div>')
            .addClass('textBoxZone')
            .append($textBoxZone1,$textBoxZone2);
        let $floatnone = $('<div>') // div to clear float
            .css('clear','both');
        calendar.append($textBoxZone,$floatnone,createCalendar());
        selectedDate();


        /**
         *Create a Calendar
         */
        function createCalendar() {
            /**
             *Navaigation bar
             */

            //Date navigation
            let $navigationDate = $('<span>')
                    .addClass('navigationDate')
                    .text("Tháng" + " " + (d.getMonth()+1) + " " + "," + " " + d.getFullYear());

            //Pre button
            let $prevbtn;
            $prevbtn = $('<span>')
                .prop('id', 'prevbtn')
                .append(
                    $('<i>')
                        .addClass('fa fa-angle-double-left')
                        .attr('title', 'previous')
                        .on('click', function () {
                            changeCalendar(-1);
                           if ($(this).closest('.left').length > 0) {
                                clickTextBox1($(this));
                            }
                            if ($(this).closest('.right').length > 0) {
                                clickTextBox2($(this));
                            }

                        })
                );

            //Next button
            let $nextbtn;
            $nextbtn = $('<span>')
                .prop('id', 'nextbtn')
                .append(
                    $('<i>')
                        .addClass('fa fa-angle-double-right')
                        .attr('title', 'next')
                        .on('click', function () {
                           changeCalendar(+1);
                            if ($(this).closest('.left').length > 0) {
                                clickTextBox1($(this));
                            }
                            if ($(this).closest('.right').length > 0) {
                                clickTextBox2($(this));
                            }

                        })
                );

            //Add pre,next,date vào Navbar
            let $navigationbar = $('<div>')
                .addClass('navigation-bar')
                .append(
                    $navigationDate,
                    $nextbtn,
                    $prevbtn
                );

            /**
             *Calendar table
             */

                //Header-table
            let $tr = $('<tr>');
            for(let i = 0; i < dayOfWeek.length; i++) {
                $tr.append(
                    $('<th>').text(dayOfWeek[i])
                );
            }
            let $header = $('<thead>')
                .addClass('header-table')
                .append($tr);


                //Body-table
            //present Month
            let $body   = $('<tbody>');
            let firstDateOfMonth        = new Date(d.getFullYear(), d.getMonth(), 1),
                firstDayOfMonth         = firstDateOfMonth.getDay(),
                lastDayOfMonth          = new Date(d.getFullYear(), d.getMonth()+1, 0),
                numberDateInMonth       = lastDayOfMonth.getDate();

            //Last Month
            let lastDayOfLastMonth      = new Date(d.getFullYear(),d.getMonth(),0),
                numberDateInLastMonth   = lastDayOfLastMonth.getDate();

            for (let i = 0 ; i < 5 ; i++) {
                $tr = $('<tr>'); // Create row

                for (let j = 0; j < 7 ; j++) { //Create column
                    let indexOfBox = i*7+j; //Number of Box
                    let dateOfCalendar, typeOfMonth;

                    //Calendar of last Month
                    if (indexOfBox < firstDayOfMonth) {
                        dateOfCalendar  = numberDateInLastMonth - firstDayOfMonth + indexOfBox + 1;
                        typeOfMonth = new Date(d.getFullYear(), d.getMonth()-1, dateOfCalendar);
                    }

                    //Calendar of pre Month
                    else if (indexOfBox - firstDayOfMonth < numberDateInMonth) {
                        dateOfCalendar  = indexOfBox - firstDayOfMonth + 1;
                        typeOfMonth = new Date(d.getFullYear(), d.getMonth(), dateOfCalendar);
                    }

                    //Calendar of next Month
                    else {
                        dateOfCalendar  = indexOfBox - firstDayOfMonth - numberDateInMonth + 1;
                        typeOfMonth = new Date(d.getFullYear(), d.getMonth()+1, dateOfCalendar);
                    }
                    let $td = $('<td>')
                        .append(
                            $('<div>')
                                .addClass('dateOfCalendar')
                                .data({'year':typeOfMonth.getFullYear(),'month':typeOfMonth.getMonth(),'date':typeOfMonth.getDate()})
                                .text(dateOfCalendar)
                        );
                    $tr
                        .append($td);
                }

                    $body.append($tr);
            }
            //Add header & body vào table
            let $table = $('<table>')
                .addClass('table')
                .append($header,$body);

            let $tab = $('<div>')
                .addClass('tab')
                .append($navigationbar,$table);
            calendar.append($tab);

        }

        /**
         * selected date
         */
        function selectedDate() {
            $('.dateOfCalendar').click(function () {
                let $self           = $(this),
                    $selfCalendar   = $self.closest(calendar),
                    $naviDate       = $selfCalendar.find('.navigationDate'),
                    $textb1          = $selfCalendar.find('.textBox1'),
                    $textb2          = $selfCalendar.find('.textBox2'),
                    $tab            = $self.closest('.tab');
                let selectedDate    = new Date($self.data('year'), $self.data('month'), $self.data('date'));
                let dateTextBox1   = new Date($textb1.data('month') + '/' + $textb1.data('date') + '/' + $textb1.data('year')),
                    dateTextBox2   = new Date($textb2.data('month') + '/' + $textb2.data('date') + '/' + $textb2.data('year')),
                    dateSelected    = new Date((selectedDate.getMonth()+1) + '/' + selectedDate.getDate() + '/' + selectedDate.getFullYear());

                $('.tab').hide();
                $naviDate.html('').text("Tháng" + " " + (selectedDate.getMonth()+1) + " " + "," + " " + selectedDate.getFullYear());
                //Click Textbox1
                if ($tab.hasClass('left')) {
                    //find diff between 2 day
                    let timeDiff = Math.abs(dateSelected.getTime() - dateTextBox1.getTime());
                    let diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));

                    //TextBox1 change
                    $textb1
                        .data({
                            year: selectedDate.getFullYear(),
                            month: selectedDate.getMonth() + 1,
                            date: selectedDate.getDate()
                        })
                        .attr({'value': selectedDate.getDate() + " " + "tháng" + " " + (selectedDate.getMonth() + 1) + " " + "," + " " + selectedDate.getFullYear()});

                    //TextBox2 change
                    if (dateSelected.getTime() >= dateTextBox1.getTime()) {
                        dateTextBox2.setDate(dateTextBox2.getDate() + diffDays);
                    }
                    else {
                        dateTextBox2.setDate(dateTextBox2.getDate() - diffDays);
                    }
                    $textb2
                        .data({
                            'date': dateTextBox2.getDate(),
                            'month': dateTextBox2.getMonth() + 1,
                            'year': dateTextBox2.getFullYear()
                        })
                        .attr({'value': dateTextBox2.getDate() + " " + "tháng" + " " + (dateTextBox2.getMonth() + 1) + " " + "," + " " + dateTextBox2.getFullYear()});
                }

                //Click Textbox2
                else {
                    //Text box 2 change
                    $textb2
                        .data({
                            year:   selectedDate.getFullYear(),
                            month:  selectedDate.getMonth() + 1,
                            date:   selectedDate.getDate()
                        })
                        .attr({'value':selectedDate.getDate() + " " + "tháng" + " " + (selectedDate.getMonth() + 1) + " " + "," + " " + selectedDate.getFullYear()});

                    //Change color of text box2
                    if(selectedDate.getTime() < dateTextBox1.getTime()) {
                        $textb2.css('background-color','red');
                    }
                    else {
                        $textb2.css('background-color','');
                    }
                }

            })
        }
        

        /**
         * display Calendar
         */

        //Position calendar when click text box1
        function clickTextBox1(thistb) {
            thistb.closest(calendar).find('.tab')
                .show()
                .removeClass("right")
                .addClass("left");
        }

        //Position calendar when click text box2
        function clickTextBox2(thistb) {
            thistb.closest(calendar).find('.tab')
                .show()
                .removeClass("left")
                .addClass("right");
        }


        /**
         * Create mouseup event
         */
        $(document).mouseup(function (e) {
            let $tab = $('.tab');
            if (!$tab.is(e.target) // if the target of the click isn't the container...
                && $tab.has(e.target).length === 0) // ... nor a descendant of the container
            {
                $tab.hide();
            }
        });

        /**
         * prev,next btn
         * @param changeNumber
         */
        function changeCalendar(changeNumber) {
            let firstDateOfMonth = new Date(d.getFullYear(), d.getMonth(), 1) ;
            let firstDateOfNextMonth = new Date(firstDateOfMonth.getTime());
            firstDateOfNextMonth.setMonth(firstDateOfNextMonth.getMonth() + changeNumber);
            d = new Date(firstDateOfNextMonth.getFullYear(),firstDateOfNextMonth.getMonth(),firstDateOfNextMonth.getDate());
            createCalendar();
            selectedDate();
        }
    }
})(jQuery);