$(document).ready(function() {
    let d = new Date();
    function ngay() {
        let $button1 = $("<input>")
            .addClass("btn but1")
            .attr({'value':d.getDate() + " " + "tháng" + " " + Number(d.getMonth() + 1) + " " + "," + " " + d.getFullYear()})
            .on("click", function () {
                but1()
            })
            .data({"date": d.getDate(), "month": d.getMonth() + 1, "year": d.getFullYear()});
        const $span = $("<span>")
            .text("TO");
        let $button2 = $("<input>")
            .addClass("btn but2")
            .attr({'value':d.getDate() + " " + "tháng" + " " + Number(d.getMonth() + 1) + " " + "," + " " + d.getFullYear()})
            .on("click", function () {
                but2()
            })
            .data({"date":d.getDate(), "month":d.getMonth()+1, "year":d.getFullYear()});
        $(".date").append($button1).append($span).append($button2);
        function but1() {
            $(".tab").show().css({"position":"absolute","top":"55px","left":"349px"}).removeClass("right").addClass("left");
        }
        function but2() {
            $(".tab").show().css({"position":"absolute","top":"55px","left":"590px"}).removeClass("left").addClass("right");
        }
        $(document).mouseup(function (e) {
            let $tab = $('.tab');
            if (!$tab.is(e.target) // if the target of the click isn't the container...
                && $tab.has(e.target).length === 0) // ... nor a descendant of the container
            {
                $tab.hide();
            }
        });
    }
    ngay();
    function  lich() {
        $('.ngaycualich').click(function() {
            let $self = $(this);
            let $btn1 = $('.but1'),
                $btn2 = $('.but2'),
                $tab  = $self.closest('.tab');
            let selectedDate = new Date($self.data('year'), $self.data('month') - 1, $self.data('date'));
            $tab.hide();
            $('.headertable').find('span').html('').text("Tháng" + " " + (selectedDate.getMonth()+1) + " " + "," + " " + selectedDate.getFullYear());
            //Click button 1
            if ($tab.hasClass('left')) {
                let oldDate2    = $btn2.data('date'),
                    oldMonth2   = $btn2.data('month'),
                    oldYear2    = $btn2.data('year');
                let but2Old     = new Date(oldYear2, oldMonth2-1, oldDate2);
                let oldDate1    = $btn1.data('date'),
                    oldMonth1   = $btn1.data('month'),
                    oldYear1    = $btn1.data('year');
                let date1 = new Date(oldMonth1 + '/' + oldDate1 + '/' + oldYear1);
                let date2 = new Date((selectedDate.getMonth()+ 1) + "/" + selectedDate.getDate() + "/" + selectedDate.getFullYear());
                let timeDiff = Math.abs(date2.getTime() - date1.getTime());
                let diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));

                //button 1
                $btn1
                    .data({
                        year:   selectedDate.getFullYear(),
                        month:  selectedDate.getMonth() + 1,
                        date:   selectedDate.getDate()
                    })
                    .attr({'value':selectedDate.getDate() + " " + "tháng" + " " + (selectedDate.getMonth() + 1) + " " + "," + " " + selectedDate.getFullYear()});


                //button 2
                let dateBtn2 = new Date($btn2.data('year'),$btn2.data('month')-1,$btn2.data('date'));
                if (date2.getTime() >= date1.getTime()) {
                    dateBtn2.setDate(but2Old.getDate() + diffDays);
                }
                else {
                    dateBtn2.setDate(but2Old.getDate() - diffDays);
                }
                $btn2
                    .data({'date':dateBtn2.getDate(),'month':dateBtn2.getMonth()+1,'year':dateBtn2.getFullYear()})
                    .attr({'value':dateBtn2.getDate() + " " + "tháng" + " " + (dateBtn2.getMonth()+1) + " " + "," + " " + dateBtn2.getFullYear()});
            }

            //Click button 2
           /* else {
                $btn2
                    .data({
                        year:   selectedDate.getFullYear(),
                        month:  selectedDate.getMonth() + 1,
                        date:   selectedDate.getDate()
                    })
                    .attr({'value':selectedDate.getDate() + " " + "tháng" + " " + (selectedDate.getMonth() + 1) + " " + "," + " " + selectedDate.getFullYear()});
                let oldDate1    = $btn1.data('date'),
                    oldMonth1   = $btn1.data('month'),
                    oldYear1    = $btn1.data('year');
                let date3 = new Date(selectedDate.getFullYear(), selectedDate.getMonth(), selectedDate.getDate());
                let date1 = new Date(oldMonth1 + '/' + oldDate1 + '/' + oldYear1);
                if (date3.getTime() < date1.getTime()) {
                    $btn2.css({'background-color':'red'})
                }
                else {
                    $btn2.css({'background-color':''})
                }
            }*/
        })
    }

    function thaydoilich(abc) {
        let a = new Date(d.getFullYear(), d.getMonth(), 1) ;
        let b = new Date(a.getTime());
        b.setMonth(b.getMonth() + abc);
        d = new Date(b.getFullYear(), b.getMonth());
        tab();
        lich()
    }

    function tab() {
        let $tr = $("<tr>");
        let $headertable = $("<div>")
            .addClass("headertable")
            .append(
                $("<span>").text("Tháng" + " " + (d.getMonth()+1) + " " + "," + " " + d.getFullYear())
            )
            .append(
                $("<div>")
                    .append(
                        $("<a>").attr({"href":"#"}).text("<")
                            .on("click", function() {
                                thaydoilich(-1)
                            })
                    )
                    .append(
                        $("<a>").attr({"href":"#"}).text(">").on("click", function() {
                            thaydoilich(+1)
                        })
                    )
            );
        let $body = $("<tbody>");
        let ngaydauthang = new Date(d.getFullYear(), d.getMonth(), 1);
        let thudautiencuathang = ngaydauthang.getDay();
        let ngaycuoithang = new Date(d.getFullYear(), d.getMonth()+1, 0);
        let songaytrongthang = ngaycuoithang.getDate();
        let ngaycuoithangtruoc = new Date(d.getFullYear(), d.getMonth(), 0);
        let songaytrongthangtruoc = ngaycuoithangtruoc.getDate();
        for (let i = 0; i < 5; i ++) {
            $tr = $("<tr>");
            for (let j = 0; j < 7; j++) {
                let text,
                    last;
                let sothutucuaolich = i*7 + j;
                if (sothutucuaolich < thudautiencuathang) {
                    text = songaytrongthangtruoc - thudautiencuathang + sothutucuaolich +1;
                    last = new Date(d.getFullYear(),d.getMonth()-1,text);
                }
                else if (sothutucuaolich - thudautiencuathang < songaytrongthang) {
                    text = sothutucuaolich - thudautiencuathang + 1;
                    last = new Date(d.getFullYear(),d.getMonth(),text);
                }
                else {
                    text = sothutucuaolich - thudautiencuathang - songaytrongthang + 1;
                    last = new Date(d.getFullYear(),d.getMonth()+1,text);
                }
                let $td;
                $td = $("<td>")
                    .append(
                        $("<div>").text(text).addClass("ngaycualich").data({"date": last.getDate(),"month":last.getMonth()+1,"year":last.getFullYear()})
                    );
                 $tr.append($td);
            }
            $body.append($tr)
        }
        let $table = $("<table>")
            .addClass("table-borderless")
            .append(
                $("<thead>")
                    .append(
                        $("<tr>")
                            .append(
                                $("<th>").text("Cn")
                            )
                            .append(
                                $("<th>").text("T2")
                            )
                            .append(
                                $("<th>").text("T3")
                            )
                            .append(
                                $("<th>").text("T4")
                            )
                            .append(
                                $("<th>").text("T5")
                            )
                            .append(
                                $("<th>").text("T6")
                            )
                            .append(
                                $("<th>").text("T7")
                            )
                    )
            )
            .append($body);
            $(".tab").html("").append($headertable).append($table);
    }
    tab();
    lich();
});